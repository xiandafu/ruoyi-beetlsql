queryByCondition
===
	select 
	@pageTag(){
	*
	@}
	from sys_job_log where 1=1
	@if(!isBlank(jobName)){
	    and  job_name like #'%'+jobName+'%'#
	@}
	@if(!isBlank(jobGroup)){
	    and  job_group = #jobGroup#
	@}
	@if(!isBlank(status)){
	    and  status = #status#
	@}
	@if(!isBlank(invokeTarget)){
	    and  invoke_target like #'%'+invokeTarget+'%'#
	@}
	@if(has(beginTime) && beginTime != null){
	   and  create_time>= #beginTime#
	@}
	@if(has(beginTime) && endTime != null){
		and  create_time<= #endTime#
	@}
	
	@pageIgnoreTag(){
	order by job_log_id desc
	@}
	
	