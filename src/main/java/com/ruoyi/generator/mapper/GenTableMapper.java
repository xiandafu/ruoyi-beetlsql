package com.ruoyi.generator.mapper;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.db.OracleStyle;
import org.beetl.sql.core.db.SqlServerStyle;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.ruoyi.core.database.SqlParam;
import com.ruoyi.generator.domain.GenTable;
import com.ruoyi.generator.domain.GenTableColumn;

/**
 * 业务 数据层
 * 
 * @author ruoyi
 */
@SqlResource("system.gentable")
public interface GenTableMapper extends BaseMapper<GenTable>
{
    /**
     * 查询业务列表
     * 
     * @param genTable 业务信息
     * @return 业务集合
     */
	public PageQuery<GenTable> queryByCondition(PageQuery<GenTable> pageQuery);
	
    public default List<GenTable> selectGenTableList(GenTable genTable){
    	PageQuery<GenTable> page = new PageQuery<>();
    	page.setPageSize(Integer.MAX_VALUE);
        page.setPageNumber(1);
        page.setTotalRow(Integer.MAX_VALUE);
        page.setParas(genTable);
        return queryByCondition(page).getList();
    }

    
    public default PageQuery<GenTable> queryDbTableByCondition(PageQuery<GenTable> pageQuery){
    	if(this.getSQLManager().getDbStyle() instanceof OracleStyle) {
    		return this.getSQLManager().pageQuery("system.gentable.selectDbTableList_oracle", GenTable.class, pageQuery);
    	}
    	if(this.getSQLManager().getDbStyle() instanceof SqlServerStyle) {
    		return this.getSQLManager().pageQuery("system.gentable.selectDbTableList_sqlserver", GenTable.class, pageQuery);
    	}
    	return this.getSQLManager().pageQuery("system.gentable.selectDbTableList_mysql", GenTable.class, pageQuery);
    }
    
    /**
     * 查询据库列表
     * 
     * @param genTable 业务信息
     * @return 数据库表集合
     */
    public default List<GenTable> selectDbTableList(GenTable genTable){
    	PageQuery<GenTable> page = new PageQuery<>();
    	page.setPageSize(Integer.MAX_VALUE);
        page.setPageNumber(1);
        page.setTotalRow(Integer.MAX_VALUE);
        page.setParas(genTable);
        return queryByCondition(page).getList();
    }

    /**
     * 查询据库列表
     * 
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    public default List<GenTable> selectDbTableListByNames(String[] tableNames){
    	if(this.getSQLManager().getDbStyle() instanceof OracleStyle) {
    		return this.getSQLManager().select("system.gentable.selectDbTableListByNames_oracle", GenTable.class, SqlParam.create().set("tableNames", tableNames));
    	}
    	if(this.getSQLManager().getDbStyle() instanceof SqlServerStyle) {
    		return this.getSQLManager().select("system.gentable.selectDbTableListByNames_sqlserver", GenTable.class, SqlParam.create().set("tableNames", tableNames));
    	}
    	return this.getSQLManager().select("system.gentable.selectDbTableListByNames_mysql", GenTable.class, SqlParam.create().set("tableNames", tableNames));
    }

    /**
     * 查询表ID业务信息
     * 
     * @param id 业务ID
     * @return 业务信息
     */
    
    public default GenTable selectGenTableById(Long id) {
    	GenTable genTable = this.single(id);
    	List<GenTableColumn> columns = this.getSQLManager().execute("select * from gen_table_column where table_id = #tableId# ", GenTableColumn.class, SqlParam.create().set("tableId", id));
    	genTable.setColumns(columns);
    	return genTable;
    }
    
    
    default List<GenTableColumn> selectGenTableColumns(Long tableId) {
    	List<GenTableColumn> columns = this.getSQLManager().execute("select * from gen_table_column where table_id = #tableId# ", GenTableColumn.class, SqlParam.create().set("tableId", tableId));
    	return columns;
    }

    /**
     * 查询表名称业务信息
     * 
     * @param tableName 表名称
     * @return 业务信息
     */
    public default GenTable selectGenTableByName(String tableName) {
    	GenTable genTable = this.execute("select * from gen_table where table_name = ?" , tableName).get(0);
    	List<GenTableColumn> columns = this.getSQLManager().execute("select * from gen_table_column where table_id = #tableId# ", GenTableColumn.class, SqlParam.create().set("tableId", genTable.getTableId()));
    	genTable.setColumns(columns);
    	return genTable;
    }

    /**
     * 新增业务
     * 
     * @param genTable 业务信息
     * @return 结果
     */
    public default int insertGenTable(GenTable genTable) {
    	genTable.setCreateTime(new Date());
    	this.insert(genTable , true);
    	return 1;
    }

    /**
     * 修改业务
     * 
     * @param genTable 业务信息
     * @return 结果
     */
    public default int updateGenTable(GenTable genTable) {
    	genTable.setUpdateTime(new Date());
    	return this.updateTemplateById(genTable);
    }

    /**
     * 批量删除业务
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public default int deleteGenTableByIds(Long[] ids) {
    	return this.getSQLManager().executeUpdate("delete from gen_table where table_id in (#join(ids)#)", SqlParam.create().set("ids", ids));
    }

	
}