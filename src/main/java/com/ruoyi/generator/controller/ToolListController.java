package com.ruoyi.generator.controller;

import java.util.Date;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.core.web.PrimitiveController;
import com.ruoyi.generator.domain.GenTable;
import com.ruoyi.generator.mapper.GenTableMapper;
import com.ruoyi.system.domain.SysOperLog;
import com.ruoyi.system.mapper.SysOperLogMapper;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

@Controller
@RequestMapping("/tool/list")
public class ToolListController extends PrimitiveController{

	@Autowired
	GenTableMapper genTableMapper;

	@RequiresPermissions("tool:gen:list")
    @PostMapping("/gen")
    @ResponseBody
    public TableDataInfo gen(@RequestParam Map<String,Object> params){
    
		PageQuery<GenTable> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		String end = (String) params.remove("params[endTime]");
		params.put("endTime", StrUtil.isBlank(end)? null:new Date(DateUtil.parseDate(end).getTime()));
		
		String begin = (String) params.remove("params[beginTime]");
		params.put("beginTime", StrUtil.isBlank(begin)? null:new Date(DateUtil.parseDate(begin).getTime()));
		
		pageQuery.setParas(params);
        return getDataTable(genTableMapper.queryByCondition(pageQuery));
    }
	
	@RequiresPermissions("tool:gen:list")
	@PostMapping("/gen/db")
    @ResponseBody
    public TableDataInfo gendb(@RequestParam Map<String,Object> params){
    
		PageQuery<GenTable> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		
		pageQuery.setParas(params);
        return getDataTable(genTableMapper.queryDbTableByCondition(pageQuery));
    }
	
}
