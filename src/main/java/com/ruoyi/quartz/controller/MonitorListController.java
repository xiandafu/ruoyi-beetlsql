package com.ruoyi.quartz.controller;

import java.util.Date;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.core.web.PrimitiveController;
import com.ruoyi.quartz.domain.SysJob;
import com.ruoyi.quartz.domain.SysJobLog;
import com.ruoyi.quartz.mapper.SysJobLogMapper;
import com.ruoyi.quartz.mapper.SysJobMapper;
import com.ruoyi.system.domain.SysLogininfor;
import com.ruoyi.system.domain.SysOperLog;
import com.ruoyi.system.domain.SysUserOnline;
import com.ruoyi.system.mapper.SysLogininforMapper;
import com.ruoyi.system.mapper.SysOperLogMapper;
import com.ruoyi.system.mapper.SysUserOnlineMapper;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

@Controller
@RequestMapping("/monitor/list")
public class MonitorListController extends PrimitiveController{
	
	@Autowired
	SysOperLogMapper sysOperLogMapper;
	

	@Autowired
	SysLogininforMapper sysLogininforMapper;

	@Autowired
	SysUserOnlineMapper sysUserOnlineMapper;

	@Autowired
	SysJobMapper sysJobMapper;
	
	@Autowired
	SysJobLogMapper sysJobLogMapper;
	
	@RequiresPermissions("monitor:operlog:list")
    @PostMapping("/operlog")
    @ResponseBody
    public TableDataInfo operlog(@RequestParam Map<String,Object> params){
    
		PageQuery<SysOperLog> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		String end = (String) params.remove("params[endTime]");
		params.put("endTime", StrUtil.isBlank(end)? null:new Date(DateUtil.parseDate(end).getTime()));
		
		String begin = (String) params.remove("params[beginTime]");
		params.put("beginTime", StrUtil.isBlank(begin)? null:new Date(DateUtil.parseDate(begin).getTime()));
		
		pageQuery.setParas(params);
        return getDataTable(sysOperLogMapper.queryByCondition(pageQuery));
    }


	@RequiresPermissions("monitor:logininfor:list")
    @PostMapping("/logininfor")
    @ResponseBody
    public TableDataInfo logininfor(@RequestParam Map<String,Object> params){
    
		PageQuery<SysLogininfor> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		String end = (String) params.remove("params[endTime]");
		params.put("endTime", StrUtil.isBlank(end)? null:new Date(DateUtil.parseDate(end).getTime()));
		
		String begin = (String) params.remove("params[beginTime]");
		params.put("beginTime", StrUtil.isBlank(begin)? null:new Date(DateUtil.parseDate(begin).getTime()));
		
		pageQuery.setParas(params);
        return getDataTable(sysLogininforMapper.queryByCondition(pageQuery));
    }


	@RequiresPermissions("monitor:online:list")
    @PostMapping("/online")
    @ResponseBody
    public TableDataInfo online(@RequestParam Map<String,Object> params){
		PageQuery<SysUserOnline> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		pageQuery.setParas(params);
        return getDataTable(sysUserOnlineMapper.queryByCondition(pageQuery));
    }

	@RequiresPermissions("monitor:job:list")
    @PostMapping("/job")
    @ResponseBody
    public TableDataInfo job(@RequestParam Map<String,Object> params){
		PageQuery<SysJob> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		pageQuery.setParas(params);
        return getDataTable(sysJobMapper.queryByCondition(pageQuery));
    }

	@RequiresPermissions("monitor:jobLog:list")
    @PostMapping("/jobLog")
    @ResponseBody
    public TableDataInfo jobLog(@RequestParam Map<String,Object> params){
		PageQuery<SysJobLog> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		pageQuery.setParas(params);
        return getDataTable(sysJobLogMapper.queryByCondition(pageQuery));
    }
	
}
