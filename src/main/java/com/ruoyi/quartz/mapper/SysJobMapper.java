package com.ruoyi.quartz.mapper;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.annotatoin.Sql;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.ruoyi.core.database.SqlParam;
import com.ruoyi.quartz.domain.SysJob;

/**
 * 调度任务信息 数据层
 * 
 * @author ruoyi
 */
@SqlResource("system.sysjob")
public interface SysJobMapper extends BaseMapper<SysJob>
{
    /**
     * 查询调度任务日志集合
     * 
     * @param job 调度信息
     * @return 操作日志集合
     */
	public PageQuery<SysJob> queryByCondition(PageQuery<SysJob> pageQuery);
	
    public default List<SysJob> selectJobList(SysJob job){
    	PageQuery<SysJob> page = new PageQuery<>();
    	page.setPageSize(Integer.MAX_VALUE);
        page.setPageNumber(1);
        page.setTotalRow(Integer.MAX_VALUE);
        page.setParas(job);
        return queryByCondition(page).getList();
    }

    /**
     * 查询所有调度任务
     * 
     * @return 调度任务列表
     */
    public default List<SysJob> selectJobAll(){
    	return this.all();
    }

    /**
     * 通过调度ID查询调度任务信息
     * 
     * @param jobId 调度ID
     * @return 角色对象信息
     */
    @Sql("select * from sys_job where job_id = ?")
    public  SysJob selectJobById(Long jobId) ;

    /**
     * 通过调度ID删除调度任务信息
     * 
     * @param jobId 调度ID
     * @return 结果
     */
    public default int deleteJobById(Long jobId) {
    	return this.deleteById(jobId);
    }

    /**
     * 批量删除调度任务信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public default int deleteJobByIds(Long[] ids) {
    	return this.getSQLManager().executeUpdate("delete from sys_job where job_id  in (#join(ids)#)", SqlParam.create().set("ids", ids));
    }

    /**
     * 修改调度任务信息
     * 
     * @param job 调度任务信息
     * @return 结果
     */
    public default int updateJob(SysJob job) {
    	return this.updateTemplateById(job);
    }

    /**
     * 新增调度任务信息
     * 
     * @param job 调度任务信息
     * @return 结果
     */
    public default int insertJob(SysJob job) {
    	job.setCreateTime(new Date());
    	this.insert(job,true);
    	return 1;
    }

	
}
